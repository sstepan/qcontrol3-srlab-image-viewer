#!/usr/bin/env python3
# -*- mode: Python; coding: utf-8 -*-

from qcontrol3.gui.qt import qt
from qcontrol3.gui.qt.app import mainwindow

class MainWindow(mainwindow.MainWindow):
    def _setup_actions(self):
        super()._setup_actions()

        action_open = qt.QtWidgets.QAction('Open', self)
        action_open.setShortcut('Ctrl+O')
        action_open.setStatusTip('Open file')
        action_open.triggered.connect(self._open_file)
        self._actions['open'] = action_open

        action_refresh = qt.QtWidgets.QAction('Refresh', self)
        action_refresh.setShortcut('Ctrl+R')
        action_refresh.setStatusTip('Refresh images')
        action_refresh.triggered.connect(self._model.refresh)
        self._actions['refresh'] = action_refresh

        action_reset = qt.QtWidgets.QAction('Reset', self)
        action_reset.setShortcut('Ctrl+Shift+R')
        action_reset.setStatusTip('Reset all images settings')
        action_reset.triggered.connect(self._model.reset)
        self._actions['reset'] = action_reset

        action_reset_zoom = qt.QtWidgets.QAction('Reset zoom', self)
        action_reset_zoom.setShortcut('R')
        action_reset_zoom.setStatusTip('Reset zoom')
        action_reset_zoom.triggered.connect(self._model.reset_zoom)
        self._actions['reset_zoom'] = action_reset_zoom

    def _open_file(self):
        default_folder = self._model.get_config('gui', 'default_folder')
        fname, *_ = qt.QtWidgets.QFileDialog.getOpenFileName(self, 'Open file', 
                                           default_folder,"HDF5 files (*.hdf5)")
        if fname is not None and fname != '':
            self._model.subscriber.newFileEvent.emit(fname)

    def _setup_menu_file(self):
        # super()._setup_menu_file()
        m = self.menuBar()
        fm = m.addMenu('&File')
        fm.addAction(self._actions['open'])
        fm.addAction(self._actions['refresh'])
        fm.addAction(self._actions['reset'])
        fm.addAction(self._actions['reset_zoom'])
        fm.addAction(self._actions['exit'])
