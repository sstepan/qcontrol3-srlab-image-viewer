#!/usr/bin/env python3
# -*- mode: Python; coding: utf-8 -*-

from qcontrol3.gui.qt import qt
from qcontrol3.gui.qt.app import page
from qcontrol3.gui.qt.widget.logging import logtable

import userwidget

class MainPage(page.MainPage):
    NAME = "Images"

    def _create_top_widget(self):
        return userwidget.UserWidget(self, self._model)

    def _create_bottom_widget(self):
        return page.DebugPage(self, self._model)

class RawImagesPage(MainPage):
    NAME = "Raw images"

    def _create_top_widget(self):
        return userwidget.UserWidget(self, self._model, use_default_plotter=True)

class PreferencesPage(page.MainWidgetPage):
    NAME = "Preferences"

    def _initialize_ui(self):
        pass

    def _save_geometry(self):
        pass

# page.py ends here
