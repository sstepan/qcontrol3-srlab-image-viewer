#!/usr/bin/env python3
# -*- mode: Python; coding: utf-8 -*-
import h5py
import numpy as np
from scipy.ndimage import filters 
from srlab import units as u

class ImageViewer:
    def __init__(self, fig, file, settings):
        self.fig = None
        self.file = file
        self.settings = settings

        self.load_images_from_file(file)

        self.px_to_m = 10*u.Real("um")
        self.A_px = self.px_to_m**2
        self.wavelength = 461*u.Real("nm")
        self.cross_section = 3*(self.wavelength**2)/2/u.pi
        
    def load_images_from_file(self, filename):
        self.filename = filename
        with h5py.File(filename, 'r') as f:
            groups = [g for g in f.keys() if g.startswith("manta")]
            images = {}
            for g in groups:
                group = f[g]
                n_images = len(group)
                images_arr = []
                for j in range(n_images):
                    data = np.array(f['{}/{}'.format(g, j)],dtype=np.float64)
                    images_arr.append(data)
                images[g] = images_arr
            self.images = images

        return images

    def get_od_image(self, images, sigma=0):
        shadow, beam, bg = [filters.gaussian_filter(im, sigma) for im in images]
        shadow = (shadow).clip(1)
        beam = (beam).clip(1)
        od = np.log((beam/shadow).clip(0.01))
        return od

    def get_settings(self, fig, old_settings=None):
        axes = fig.axes
        settings = [(ax.get_xlim(), ax.get_ylim()) for ax in axes]
        self.update()
        return settings

    def set_settings(self, fig, settings=None):
        if settings is not None:
            axes = fig.axes
            for i, ax in enumerate(axes):
                if i<len(settings):
                    xlim, ylim = settings[i]
                    ax.set_xlim(*xlim)
                    ax.set_ylim(*ylim)

    def get_image_part(self, data, ax):
        xmin, xmax = sorted([int(v) for v in ax.get_xlim()])
        ymin, ymax = sorted([int(v) for v in ax.get_ylim()])
        if xmin < 0:
            xmin = 0
        if ymin < 0:
            ymin = 0
        return data[ymin:ymax, xmin:xmax]

    def update(self):
        pass

    def plot(self, fig, file, settings=None):
        images = self.images
        N = sum(len(images[k]) for k in images)
        nrows = max(1, len(images.keys()))
        ncols = max([1]+[len(images[k]) for k in images])
        if N > 0:
            for i, k in enumerate(images.keys()):
                for j, img in enumerate(images[k]):
                    ax = fig.add_subplot(nrows, ncols, i*ncols + j + 1)
                    ax.imshow(img,
                      vmin = -np.max(img),
                      vmax = np.max(img),
                      origin = 'lower',
                      cmap = 'RdBu'
                    )
                    ax.set_title("%s: Max: %d, total: %d" % (k, np.max(img), np.sum(img)))
        fig.subplots_adjust(left=0.05, bottom=0.05, top=0.95, right=0.95)

        if settings is not None:
            self.set_settings(fig, settings)

