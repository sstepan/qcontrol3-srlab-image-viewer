#!/usr/bin/env python3
# -*- mode: Python; coding: utf-8 -*-

from qcontrol3.gui.qt.widget.matplotlib import plotcanvas
from qcontrol3.gui.qt.layout import layout
from qcontrol3.gui.qt.app import page

# import numpy as np
# import matplotlib.pyplot as plt
# import srlab.plot.pyplot as plt

class PlotWidget(plotcanvas.PlotWidget):
    def __init__(self, *args, use_default_plotter=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.use_default_plotter = use_default_plotter
        self._model.subscriber.newFileEvent.connect(self._on_new_file)
        self._model.subscriber.redrawEvent.connect(self._redraw)

    def _on_new_file(self, file):
        self.clear()
        self._model.plot(self._fig, file, use_default=self.use_default_plotter)
        self.draw()

    def _redraw(self):
        self.draw()

    def plot(self, data=None):
        pass

    def draw_event(self, *args, **kwargs):
        super().draw_event(*args, **kwargs)
        self._model.update_plot_settings(self._fig)

class UserWidget(page.MainWidgetPage):
    def __init__(self, *args, use_default_plotter=False, **kwargs):
        self.use_default_plotter = use_default_plotter
        super().__init__(*args, **kwargs)

    def _initialize_ui(self):
        plot = PlotWidget(self, self._model, use_default_plotter=self.use_default_plotter)
        self._plot = plot
        nav = plotcanvas.NavigationToolbar(plot, self)
        nav.setStyleSheet("background-color: #eee;")
        grid = layout.VBoxLayout(spacing=0)
        grid.addWidget(nav)
        grid.addWidget(plot)
        self.setLayout(grid)

# userwidget.py ends here
