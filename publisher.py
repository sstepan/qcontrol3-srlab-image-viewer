#!/usr/bin/env python3
# -*- mode: Python; coding: utf-8 -*-

from qcontrol3.tools import qpyro
from qcontrol3.tools.EventService4 import Clients
from qcontrol3.server import timingsystemevent
import sys
import os

qpyro.configurepyro()

file = os.path.abspath(sys.argv[-1])

print(file)
p = Clients.Publisher()
ev = timingsystemevent.TimingSystemEvent(
                 ev_type='EV_SYSTEMSTATUS',
                 ev_subtype='EVSYS_DATAWRITTEN',
                 ev_source='test',
                 ev_data=[file])
p.publish('qc3server', ev)