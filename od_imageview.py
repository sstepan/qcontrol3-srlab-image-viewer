#!/usr/bin/env python
# -*- mode: Python; coding: utf-8 -*-

import h5py
import numpy as np
from scipy.ndimage import filters 
from srlab import units as u

import viewer
import srlab.fit.imagefit
import srlab.fit.func.gauss2d

class ImageViewer(viewer.ImageViewer):
    fit_func = srlab.fit.func.gauss2d.Gauss2D()
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.px_to_m = [10e-6, 10e-6]
        if "files" not in self.settings:
            self.settings["files"] = []
        if "data" not in self.settings:
            self.settings["data"] = {}
        if self.file not in self.settings["files"]:
            self.settings["files"].append(self.file)
            self.settings["data"][self.file] = [{},{}]

    def get_od_images(self):
        cameras = ["manta0", "manta1"]
        od_images = []
        for c in cameras:
            if c in self.images and len(self.images[c]) >= 3:
                od = self.get_od_image(self.images[c])
                od_images.append(od)
        self.od_images = od_images
        return od_images

    def fit(self, od, ax, i):
        d = self.get_image_part(od, ax)
        # if image is too large to fit with 2D gaussian
        if len(d)>300 or len(d[0])>300:
            total = np.sum(d)
            params = None
        else:
            # removing background
            d = (d-np.median(d)).clip(0)

            ymin = max(0,min(*ax.get_ylim()))
            xmin = max(0,min(*ax.get_xlim()))

            if not hasattr(ax,'_sr_fit'):
                F = srlab.fit.imagefit.ImageFit(self.fit_func)
                F.fit(d, image_errors=1, error_model='bare')
                ax._sr_fit = F
            else:
                F = ax._sr_fit
            params = F.best_fit_parameters
            z0, a, xx, wx, yy, wy, cor = params

            xs = np.arange(len(d[0]))
            ys = np.arange(len(d))
            x,y = np.meshgrid(xs,ys)
            fit_image = F.best_fit(x,y)
            if not hasattr(ax,'_sr_cnt'):
                cnt = ax.contour(x+xmin, y+ymin, fit_image, cmap="YlOrRd", levels=[z0+a/np.e**i for i in [3,1]])
                ax._sr_cnt = cnt
            total = 2*np.pi*a*wx*wy*(1-cor**2)**0.5
            self.settings["data"][self.file][i] = {
                "total": total,
                "wx": wx,
                "wy": wy,
                "natoms": (self.px_to_m[i]**2/self.cross_section*total).value
            }
        Natoms = (self.px_to_m[i]**2/self.cross_section*total).value
        return Natoms, params

    # fires on zoom / pan and other changes
    def update(self):
        self.ax_n.clear()
        self.ax_w.clear()
        for i, d in enumerate(self.data):
            od, ax, im = d
            Natoms, params = self.fit(od, ax, i)
            if params is not None:
                z0, a, xx, wx, yy, wy, cor = params
                ax.set_title("N: %.2e, wx: %.2f, wy: %.2f" % (Natoms, wx, wy), fontsize=20)
            else:
                ax.set_title("N: %.2e" % Natoms, fontsize=20)
            d = []
            dwx = []
            dwy = []
            for file in self.settings["files"]:
                if "natoms" in self.settings["data"][file][i]:
                    d.append(self.settings["data"][file][i]["natoms"])
                    dwx.append(self.settings["data"][file][i]["wx"])
                    dwy.append(self.settings["data"][file][i]["wy"])
            dwx = np.array(dwx)*self.px_to_m[i]*1e6
            dwy = np.array(dwy)*self.px_to_m[i]*1e6
            self.ax_n.plot(d,"o", color="C{}".format(2*i), label="Image {}".format(i))
            self.ax_n.set_ylabel("Natoms")
            self.ax_n.legend()
            self.ax_w.plot(dwy,"o", color="C{}".format(2*i+1), label="Image {}, wy".format(i))
            self.ax_w.plot(dwx,"o", color="C{}".format(2*i), label="Image {}, wx".format(i))
            self.ax_w.set_ylabel("waist, um")
            self.ax_w.legend()
    # fires on new file
    def plot(self, fig, file, settings=None):
        od_images = self.get_od_images()

        vmax=3

        n = max(len(od_images), 2)
        self.data = []
        self.ax_n = fig.add_subplot(2, n, n+1)
        self.ax_w = fig.add_subplot(2, n, n+2)
        for i, od in enumerate(od_images):
            ax = fig.add_subplot(2, n, i+1)
            im = ax.imshow(od,
              vmin = -vmax,
              vmax = vmax,
              origin = 'lower',
              cmap = 'RdBu'
            )
            self.data.append( (od, ax, im) )
        
        if settings is not None:
            self.set_settings(fig, settings)

        self.update()
