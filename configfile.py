#!/usr/bin/env python3
# -*- mode: Python; coding: utf-8 -*-

from qcontrol3.tools import qc3config

class ConfigFile(qc3config.ConfigFile):

    _filename = 'image_viewer'
    _content = [
        ('app',
         'Describes the application',
         [
             (
                 'name',
                 'image_viewer',
                 'The application name',
             ),
             (
                 'version',
                 '0.2.0',
                 'The application version',
             ),
             (
                 'description',
                 'Image viewer',
                 'The application description',
             ),
             (
                 'authors',
                 [],
                 'The application authors',
             ),
             (
                 'emails',
                 [],
                 'The application authors\' email addresses',
             ),
             (
                 'organization_name',
                 'srlab',
                 'Organization name',
             ),
             (
                 'organization_domain',
                 'srlab.net',
                 'Organization domain',
             ),
         ]),
        ('gui',
         'Configures the gui',
         [
             (
                 'geometry',
                 [20, 30, 1024, 768],
                 'Window geometry as [X, Y, WIDTH, HEIGHT]',
             ),
             (
                 'main_window_title',
                 'Image viewer',
                 'Window title',
             ),
             (
                 'main_page_splitter_sizes',
                 [768, 0],
                 'Main page splitter heights'
             ),
             (
                 'default_folder',
                 '/',
                 'Default folder to look for files'
             ),
         ]),
    ]

# config.py ends here
