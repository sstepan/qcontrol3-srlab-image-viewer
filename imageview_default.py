#!/usr/bin/env python3
# -*- mode: Python; coding: utf-8 -*-

import h5py
import numpy as np

def load_images_from_file(filename):
    with h5py.File(filename, 'r') as f:
        groups = [g for g in f.keys() if g.startswith("manta")]
        images = {}
        for g in groups:
            group = f[g]
            n_images = len(group)
            images_arr = []
            for j in range(n_images):
                data = np.array(f['{}/{}'.format(g, j)],dtype=np.float64)
                images_arr.append(data)
            images[g] = images_arr
        return images

def get_settings(fig, old_settings=None):
    axes = fig.axes
    return [(ax.get_xlim(), ax.get_ylim()) for ax in axes]

def set_settings(fig, settings=None):
    if settings is not None:
        axes = fig.axes
        for i, ax in enumerate(axes):
            if i<len(settings):
                xlim, ylim = settings[i]
                ax.set_xlim(*xlim)
                ax.set_ylim(*ylim)

def plot(fig, file, settings=None):
    images = load_images_from_file(file)

    N = sum(len(images[k]) for k in images)
    nrows = max(1, len(images.keys()))
    ncols = max([1]+[len(images[k]) for k in images])
    if N > 0:
        for i, k in enumerate(images.keys()):
            for j, img in enumerate(images[k]):
                ax = fig.add_subplot(nrows, ncols, i*ncols + j + 1)
                ax.imshow(img,
                  vmin = -np.max(img),
                  vmax = np.max(img),
                  origin = 'lower',
                  cmap = 'RdBu'
                )
                ax.set_title("%s: Max: %d, total: %d" % (k, np.max(img), np.sum(img)))
    fig.subplots_adjust(left=0.05, bottom=0.05, top=0.95, right=0.95)

    if settings is not None:
        set_settings(fig, settings)

    return get_settings
