#!/usr/bin/env python3
# -*- mode: Python; coding: utf-8 -*-

import sys
import Pyro4

from qcontrol3.gui.qt import qt
import qcontrol3.tools.qpyro
from qcontrol3.tools.EventService4 import Clients

class EventServerSubscriber(Clients.Subscriber):
    def __init__(self, new_file_callback):
        super().__init__()
        self._new_file_callback = new_file_callback

    @Pyro4.expose
    @Pyro4.oneway
    def event(self, event):
        e = event.msg
        if e.ev_subtype == 'EVSYS_DATAWRITTEN':
            for f in e.ev_data:
                self._new_file_callback(f)

class EventServerSubscriberThread(qt.QtCore.QThread):
    newFileEvent = qt.QtCore.pyqtSignal(str)
    dataUpdateEvent = qt.QtCore.pyqtSignal(dict)
    redrawEvent = qt.QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()
        qcontrol3.tools.qpyro.configurepyro()

        self._viewer = EventServerSubscriber(self._on_new_file)
        self._subscription = 'qc3server'
        self._viewer.subscribe(self._subscription)

    def run(self):
        sys.stdout.write('* Start event loop\n')
        sys.stdout.flush()
        self._viewer.listen()
        sys.stdout.write('* Done with event loop\n')
        sys.stdout.flush()

    def stop(self):
        self._viewer.unsubscribe(self._subscription)
        self._viewer.abort()

    def _on_new_file(self, filename):
        self.newFileEvent.emit(filename)

# subscriber.py ends here
