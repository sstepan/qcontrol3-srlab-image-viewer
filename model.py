#!/usr/bin/env python3
# -*- mode: Python; coding: utf-8 -*-

from qcontrol3.gui.qt.app import model

import subscriber
import numpy as np
from datetime import datetime
import os

from importlib import reload
import importlib.util

import imageview_default
import traceback

class Model(model.Model):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.subscriber = subscriber.EventServerSubscriberThread()
        self.subscriber.newFileEvent.connect(self._on_new_file)

        self.subscriber.dataUpdateEvent.connect(self._on_new_data)

        self._threads = [
            self.subscriber
        ]
        self.last_file = None
        self._plot_settings = None
        self._get_plot_settings = None
        self.viewer = None
        self._viewer_settings = {}

    def _on_new_file(self, file):
        self.last_file = file
        self.set_config('gui', 'default_folder', os.path.dirname(file))
        self.log(file, level='INFO')
        # self.subscriber.dataUpdateEvent.emit(self._channels)

    def refresh(self):
        if self.last_file is not None:
            self.subscriber.newFileEvent.emit(self.last_file)

    def reset(self):
        self._plot_settings = None
        self._get_plot_settings = None
        self._viewer_settings = {}
        self.viewer = None
        self.refresh()

    def reset_zoom(self):
        self._plot_settings = None
        self.refresh()

    def log_exception(self, e):
        self.log(traceback.format_exc(), level='CRITICAL')

    def update_plot_settings(self, fig):
        try:
            if self._get_plot_settings is not None:
                old_settings = self._plot_settings
                self._plot_settings = self._get_plot_settings(fig, self._plot_settings)
                if old_settings != self._plot_settings:
                    self.subscriber.redrawEvent.emit()
            if self.viewer is not None:
                old_settings = self._plot_settings
                self._plot_settings = self.viewer.get_settings(fig, self._plot_settings)
                if old_settings != self._plot_settings:
                    self.subscriber.redrawEvent.emit()
        except Exception as e:
            self.log_exception(e)

    def plot(self, fig, file, use_default=False):
        try:
            module_file = os.path.join(os.path.dirname(file), "imageview.py")
            if os.path.isfile(module_file) and not use_default:
                spec = importlib.util.spec_from_file_location("imageview", module_file)
                imageview = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(imageview)
            else:
                reload(imageview_default)
                imageview = imageview_default
            if hasattr(imageview, "plot"):
                self._get_plot_settings = imageview.plot(fig, file, settings=self._plot_settings)
                self.update_plot_settings(fig)
            elif hasattr(imageview, "ImageViewer"):
                self.viewer = imageview.ImageViewer(fig, file, self._viewer_settings)
                self.viewer.plot(fig, file, settings=self._plot_settings)
                self.update_plot_settings(fig)
        except Exception as e:
            self.log("Failed at imageview module", level='CRITICAL')
            self.log_exception(e)

    def _on_new_data(self, obj):
        self.log('New data event!')


# model.py ends here
